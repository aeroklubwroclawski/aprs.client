﻿using Serilog;
using Topshelf;

namespace APRS.NTService
{
  public class AprsServiceHost : ServiceControl
  {
    private readonly Client.AprsServiceController _controller;

    public AprsServiceHost()
    {
      _controller = new Client.AprsServiceController();
    }

    public bool Start(HostControl hostControl)
    {
      Log.Logger.Information("Starting service APRS.Client");

      _controller.Start();

      Log.Logger.Information("Service APRS.Client started");

      return true;
    }

    public bool Stop(HostControl hostControl)
    {
      Log.Logger.Information("Stopping service APRS.Client");

      _controller.Stop();

      Log.Logger.Information("Service APRS.Client stopped");

      return true;
    }
  }
}