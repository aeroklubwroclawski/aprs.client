﻿using System;
using System.Globalization;
using APRS.Client.Configuration;
using APRS.Migrations.Runners;
using Serilog;
using Serilog.Exceptions;
using Topshelf;

namespace APRS.NTService
{
  class Program
  {
    static void Main(string[] args)
    {
      Log.Logger = new LoggerConfiguration()
        .Enrich.FromLogContext()
        .Enrich.WithThreadId()
        .Enrich.WithExceptionDetails()
        .WriteTo.ColoredConsole(
          outputTemplate: "{Timestamp:yyy-MM-dd HH:mm:ss} [{Level}] [{ThreadId}] [{SourceContext}] {Message}{NewLine}{Exception}")
        .WriteTo.RollingFile(
          $@"{AppDomain.CurrentDomain.BaseDirectory}\logs\aprs.client-{{Date}}.log",
          outputTemplate: "{Timestamp:yyy-MM-dd HH:mm:ss} [{Level}] [{ThreadId}] [{SourceContext}] {Message}{NewLine}{Exception}")
        .MinimumLevel.Verbose()
        .CreateLogger();

      CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("en-US");
      CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("en-US");

      try
      {
        RuntimeMigrationRunner.RunMigrations(new SqlServerConfiguration().ConnectionString);
      }
      catch (Exception e)
      {
        Log.Logger.Error(e, "Cannot migrate database to newest version");
        throw;
      }

      HostFactory.Run(x =>
      {
        x.Service<AprsServiceHost>();

        x.UseSerilog(Log.Logger);

        x.StartAutomatically();
        x.RunAsLocalService();
      });
    }
  }
}
