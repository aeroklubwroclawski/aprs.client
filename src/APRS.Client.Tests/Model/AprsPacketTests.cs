﻿using System;
using APRS.Client.Model;
using NUnit.Framework;
using Shouldly;

namespace APRS.Client.Tests.Model
{
  public class AprsPacketTests
  {
    [Test]
    public void AprsPacket_parse_raw_header()
    {
      var payload = "ICA4B43BE>APRS,qAS,LFGB:/151531h4753.90N/00742.88EX213/081/A=001752 !W61! id0D4B43BE -078fpm -0.1rot 7.2dB 0e +0.2kHz gps2x2";

      var packet = new AprsPacket(payload);

      packet.RawHeader.ShouldBe("ICA4B43BE>APRS,qAS,LFGB");
    }

    [Test]
    public void AprsPacket_parse_raw_body()
    {
      var payload = "ICA4B43BE>APRS,qAS,LFGB:/151531h4753.90N/00742.88EX213/081/A=001752 !W61! id0D4B43BE -078fpm -0.1rot 7.2dB 0e +0.2kHz gps2x2";

      var packet = new AprsPacket(payload);

      packet.RawBody.ShouldBe("/151531h4753.90N/00742.88EX213/081/A=001752 !W61! id0D4B43BE -078fpm -0.1rot 7.2dB 0e +0.2kHz gps2x2");
    }

    [Test]
    public void AprsPacket_parse_callsign()
    {
      var payload = "ICA4B43BE>APRS,qAS,LFGB:/151531h4753.90N/00742.88EX213/081/A=001752 !W61! id0D4B43BE -078fpm -0.1rot 7.2dB 0e +0.2kHz gps2x2";

      var packet = new AprsPacket(payload);

      packet.Callsign.ShouldBe("ICA4B43BE");
    }

    [Test]
    public void AprsPacket_parse_destination()
    {
      var payload = "ICA4B43BE>APRS,qAS,LFGB:/151531h4753.90N/00742.88EX213/081/A=001752 !W61! id0D4B43BE -078fpm -0.1rot 7.2dB 0e +0.2kHz gps2x2";

      var packet = new AprsPacket(payload);

      packet.Destination.ShouldBe("APRS");
    }

    [Test]
    public void AprsPacket_parse_routing()
    {
      var payload = "ICA4B43BE>APRS,qAS,LFGB:/151531h4753.90N/00742.88EX213/081/A=001752 !W61! id0D4B43BE -078fpm -0.1rot 7.2dB 0e +0.2kHz gps2x2";

      var packet = new AprsPacket(payload);

      packet.Routing.ShouldBe("APRS,qAS,LFGB");
    }

    [Test]
    public void AprsPacket_parse_comment_packet_type()
    {
      var payload = "# some comment";

      var packet = new AprsPacket(payload);

      packet.Type.ShouldBe(AprsPacketType.Comment);
    }

    [Test]
    public void AprsPacket_parse_comment_packet()
    {
      var payload = "# some comment";

      var packet = new AprsPacket(payload);

      packet.Comment.ShouldBe("some comment");
    }

    [Test]
    public void AprsPacket_parse_packet_invalid_type()
    {
      var payload = "adgadfgs";

      var packet = new AprsPacket(payload);

      packet.Type.ShouldBe(AprsPacketType.Invalid);
    }

    [Test]
    public void AprsPacket_parse_packet_time()
    {
      var payload = "ICA4B43BE>APRS,qAS,LFGB:/151531h4753.90N/00742.88EX213/081/A=001752 !W61! id0D4B43BE -078fpm -0.1rot 7.2dB 0e +0.2kHz gps2x2";

      var packet = new AprsPacket(payload);

      packet.Time.ShouldNotBe(DateTime.MinValue);
      packet.Time.Hour.ShouldBe(15);
      packet.Time.Minute.ShouldBe(15);
      packet.Time.Second.ShouldBe(31);
    }

    [Test]
    public void AprsPacket_parse_packet_symbol()
    {
      var payload = "ICA4B43BE>APRS,qAS,LFGB:/151531h4753.90N/00742.88EX213/081/A=001752 !W61! id0D4B43BE -078fpm -0.1rot 7.2dB 0e +0.2kHz gps2x2";

      var packet = new AprsPacket(payload);

      packet.Symbol.ShouldBe("/X");
    }

    [Test]
    public void AprsPacket_parse_packet_latitude()
    {
      var payload = "ICA4B43BE>APRS,qAS,LFGB:/151531h4753.90N/00742.88EX213/081/A=001752 !W61! id0D4B43BE -078fpm -0.1rot 7.2dB 0e +0.2kHz gps2x2";

      var packet = new AprsPacket(payload);

      packet.Latitude.ShouldBe(47.8983345f);
    }

    [Test]
    public void AprsPacket_parse_packet_longitude()
    {
      var payload = "ICA4B43BE>APRS,qAS,LFGB:/151531h4753.90N/00742.88EX213/081/A=001752 !W61! id0D4B43BE -078fpm -0.1rot 7.2dB 0e +0.2kHz gps2x2";

      var packet = new AprsPacket(payload);

      packet.Longitude.ShouldBe(7.714667f);
    }

    [Test]
    public void AprsPacket_parse_packet_course()
    {
      var payload = "ICA4B43BE>APRS,qAS,LFGB:/151531h4753.90N/00742.88EX213/081/A=001752 !W61! id0D4B43BE -078fpm -0.1rot 7.2dB 0e +0.2kHz gps2x2";

      var packet = new AprsPacket(payload);

      packet.Course.ShouldBe(213);
    }

    [Test]
    public void AprsPacket_parse_packet_speed()
    {
      var payload = "ICA4B43BE>APRS,qAS,LFGB:/151531h4753.90N/00742.88EX213/081/A=001752 !W61! id0D4B43BE -078fpm -0.1rot 7.2dB 0e +0.2kHz gps2x2";

      var packet = new AprsPacket(payload);

      packet.Speed.ShouldBe(150);
    }
  }
}