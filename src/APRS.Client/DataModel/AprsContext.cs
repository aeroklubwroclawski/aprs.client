﻿using System.Data.Entity;
using APRS.Client.Model;

namespace APRS.Client.DataModel
{
  public class AprsContext : DbContext
  {
    public AprsContext()
      : base("APRS.Client")
    {
      Database.SetInitializer(new NullDatabaseInitializer<AprsContext>());
    }

    public virtual IDbSet<TrackingPoint> TrackingPoints { get; set; }

    public virtual IDbSet<UnsupportedPacket> UnsupportedPackets { get; set; }
  }
}