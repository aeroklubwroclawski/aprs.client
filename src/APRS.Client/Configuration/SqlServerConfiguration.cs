﻿using System.Configuration;

namespace APRS.Client.Configuration
{
  public struct SqlServerConfiguration
  {
    public string ConnectionString => ConfigurationManager.ConnectionStrings["APRS.Client"].ConnectionString;
  }
}