﻿using System.Configuration;

namespace APRS.Client.Configuration
{
  public class AprsSettings
  {
    public string Host => ConfigurationManager.AppSettings["aprs.host"];

    public int Port => int.Parse(ConfigurationManager.AppSettings["aprs.port"]);

    public string Login => ConfigurationManager.AppSettings["aprs.login"];

    public string Password => ConfigurationManager.AppSettings["aprs.password"];

    public string Filter => ConfigurationManager.AppSettings["aprs.filter"];
  }
}