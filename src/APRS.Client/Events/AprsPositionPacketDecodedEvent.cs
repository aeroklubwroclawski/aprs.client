﻿using APRS.Client.Model;

namespace APRS.Client.Events
{
  public class AprsPositionPacketDecodedEvent
  {
    public AprsPacket Packet { get; }

    public AprsPositionPacketDecodedEvent(AprsPacket packet)
    {
      Packet = packet;
    }
  }
}