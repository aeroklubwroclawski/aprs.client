﻿namespace APRS.Client.Events
{
  public class UnsupportedPacketReceivedEvent
  {
    public string Payload { get; }

    public UnsupportedPacketReceivedEvent(string payload)
    {
      Payload = payload;
    }
  }
}