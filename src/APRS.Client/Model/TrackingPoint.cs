﻿using System;

namespace APRS.Client.Model
{
  public class TrackingPoint
  {
    public Guid Id { get; private set; }

    public string Callsign { get; private set; }

    public DateTime Time { get; private set; }

    public float Latitude { get; private set; }

    public float Longitude { get; private set; }

    public float Altitude { get; private set; }

    public DateTime CreatedDate { get; private set; }

    public TrackingPoint(string callsign, DateTime time, float latitude, float longitude, float altitude)
    {
      Id = Guid.NewGuid();
      Callsign = callsign;
      Time = time;
      Latitude = latitude;
      Longitude = longitude;
      Altitude = altitude;
      CreatedDate = DateTime.UtcNow;
    }

    public static TrackingPoint FromAprsPacket(AprsPacket packet)
    {
      return new TrackingPoint(
        packet.Callsign,
        packet.Time,
        packet.Latitude,
        packet.Longitude,
        packet.Altitude);
    }
  }
}