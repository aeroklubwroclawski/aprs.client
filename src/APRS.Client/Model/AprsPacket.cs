﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace APRS.Client.Model
{
  public class AprsPacket
  {
    private static CultureInfo _usCulture;

    private const string RegexPacketPositionType = "^([/!@=])(.*)";

    private const string RegexPacketPositionWithoutTimstampType = "^([!=])(.*)";

    private const string RegexCommentPacket = "^#(.*)$";

    private const string RegexPacketHeader = "^([A-Za-z0-9\\-]{1,9})>(.*)$";

    private const string RegexPacketTimestamp = "([0-9]{2})([0-9]{2})([0-9]{2})([zh/])";

    private const string RegexPacketPosition = "^([0-9]{2})([0-7][0-9]\\.[0-9]{2})([NnSs])(.)([0-9]{3})([0-7][0-9]\\.[0-9]{2})([EeWw])(.)";

    private const string RegexPacketSpeedAndCourse = "^([0-9]{3})/([0-9]{3})";

    private const string RegexPacketAltitude = "^/A=([0-9]{6})";

    #region Properties

    public string RawHeader { get; private set; }

    public string RawBody { get; private set; }

    public AprsPacketType Type { get; private set; }

    public string Comment { get; private set; }

    public string Routing { get; private set; }

    public string Callsign { get; private set; }

    public string Destination { get; private set; }

    public DateTime Time { get; private set; }

    public float Latitude { get; private set; }

    public float Longitude { get; private set; }

    public string Symbol { get; private set; }

    public int Course { get; private set; }

    public int Speed { get; private set; }

    public float Altitude { get; private set; }

    #endregion

    public AprsPacket(string payload)
    {
      Parse(payload);
    }

    private void Parse(string payload)
    {
      if (Regex.IsMatch(payload, RegexCommentPacket))
      {
        ParseCommentPacket(payload);
        return;
      }

      var headerEndCharPosition = payload.IndexOf(":", StringComparison.Ordinal);

      if (headerEndCharPosition < 0)
      {
        Type = AprsPacketType.Invalid;

        return;
      }

      RawHeader = payload.Substring(0, headerEndCharPosition);
      RawBody = payload.Substring(headerEndCharPosition + 1, payload.Length - headerEndCharPosition - 1);

      ParseHeader(RawHeader);

      if (Regex.IsMatch(RawBody, RegexPacketPositionType))
      {
        ParsePositionPacket(RawBody);
        return;
      }

      Type = AprsPacketType.Unknown;
    }

    private void ParseCommentPacket(string payload)
    {
      var match = Regex.Match(payload, RegexCommentPacket);

      Comment = match.Groups[1].Value.Trim();
      Type = AprsPacketType.Comment;
    }

    private void ParseHeader(string input)
    {
      var matches = Regex.Matches(input, RegexPacketHeader);
      var groups = matches[0].Groups;

      Callsign = groups[1].Value;
      Routing = groups[2].Value;

      var index = Routing.IndexOf(",");

      Destination = Routing.Substring(0, index);
    }

    private void ParsePositionPacket(string input)
    {
      Type = AprsPacketType.Position;

      var bodyToParse = input.Substring(1, RawBody.Length - 1);

      if (Regex.IsMatch(input, RegexPacketPositionWithoutTimstampType) == false)
      {
        ParseTimestamp(bodyToParse.Substring(0, 7));

        bodyToParse = bodyToParse.Substring(7, bodyToParse.Length - 7 - 1);
      }
      else
      {
        Time = DateTime.Now;
      }

      ParsePosition(bodyToParse);

      if (Type == AprsPacketType.Invalid)
      {
        return;
      }

      bodyToParse = bodyToParse.Substring(19, bodyToParse.Length - 19 - 1);

      if (ParseCourseAndSpeed(bodyToParse))
      {
        bodyToParse = bodyToParse.Substring(7, bodyToParse.Length - 7 - 1);
      }

      ParseAltitude(bodyToParse);
    }

    private void ParseTimestamp(string input)
    {
      var matches = Regex.Matches(input, RegexPacketTimestamp);
      var groups = matches[0].Groups;

      int day, hour, minutes, seconds = 0;
      var now = DateTime.Now;

      switch (groups[4].Value)
      {
        case "z":
        case "/":
          day = int.Parse(groups[1].Value);
          hour = int.Parse(groups[2].Value);
          minutes = int.Parse(groups[3].Value);
          Time = new DateTime(now.Year, now.Month, day, hour, minutes, 0);
          break;
        case "h":
          hour = int.Parse(groups[1].Value);
          minutes = int.Parse(groups[2].Value);
          seconds = int.Parse(groups[3].Value);
          Time = new DateTime(now.Year, now.Month, now.Day, hour, minutes, seconds);
          break;
      }
    }

    private void ParsePosition(string input)
    {
      var matches = Regex.Matches(input, RegexPacketPosition);

      if (matches.Count == 0)
      {
        Type = AprsPacketType.Invalid;

        return;
      }

      var groups = matches[0].Groups;

      var latitudeDegrees = int.Parse(groups[1].Value);
      _usCulture = new CultureInfo("en-US");
      var latitudeDecimalMinutes = double.Parse(groups[2].Value, _usCulture);
      var latitudeDirection = groups[3].Value;

      var longitudeDegrees = int.Parse(groups[5].Value);
      var longitudeDecimalMinutes = double.Parse(groups[6].Value, _usCulture);
      var longitudeDirection = groups[7].Value;

      Latitude = (float)(latitudeDegrees + latitudeDecimalMinutes / 60.0);
      Longitude = (float)(longitudeDegrees + longitudeDecimalMinutes / 60.0);

      if (latitudeDirection.ToUpper() == "S")
      {
        Latitude = 0 - Latitude;
      }

      if (longitudeDirection == "W")
      {
        Longitude = 0 - Longitude;
      }

      Symbol = groups[4].Value + groups[8].Value;
    }

    private bool ParseCourseAndSpeed(string input)
    {
      if (Regex.IsMatch(input, RegexPacketSpeedAndCourse) == false)
      {
        return false;
      }

      var matches = Regex.Matches(input, RegexPacketSpeedAndCourse);
      var groups = matches[0].Groups;

      Course = int.Parse(groups[1].Value);
      Speed = ConvertToKmph(int.Parse(groups[2].Value));

      return true;
    }

    private void ParseAltitude(string input)
    {
      if (Regex.IsMatch(input, RegexPacketAltitude) == false)
      {
        return;
      }

      var matches = Regex.Matches(input, RegexPacketAltitude);
      var groups = matches[0].Groups;

      Altitude = ConvertToMeters(int.Parse(groups[1].Value));
    }

    private static int ConvertToMeters(int feets)
    {
      return (int)(feets * 12f / 39.37f);
    }

    private static int ConvertToKmph(int knots)
    {
      return (int)(knots * 1.852);
    }

    public override string ToString()
    {
      return $"Callsign = [{Callsign}]; Time = [{Time}]; Latitude = [{Latitude}]; Longitude = [{Longitude}]; Course = [{Course}]; Speed = [{Speed}]; Altitude = [{Altitude}]";
    }
  }
}