﻿namespace APRS.Client.Model
{
  public enum AprsPacketType
  {
    Comment,
    Position,
    Status,
    Invalid,
    Unknown
  }
}