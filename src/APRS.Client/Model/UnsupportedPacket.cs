﻿using System;

namespace APRS.Client.Model
{
  public class UnsupportedPacket
  {
    public Guid Id { get; private set; }

    public string Payload { get; private set; }

    public DateTime CreatedDate { get; private set; }

    public UnsupportedPacket(string payload)
    {
      Id = Guid.NewGuid();
      Payload = payload;
      CreatedDate = DateTime.Now;
    }
  }
}