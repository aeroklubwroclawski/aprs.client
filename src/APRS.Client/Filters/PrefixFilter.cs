﻿using System.Collections.Generic;

namespace APRS.Client.Filters
{
  public class PrefixFilter : IFilter
  {
    private readonly IEnumerable<string> _prefixes;

    public PrefixFilter(IEnumerable<string> prefixes)
    {
      _prefixes = prefixes;
    }

    public string GetFilterString()
    {
      return $"p/{string.Join("/", _prefixes)}";
    }
  }
}