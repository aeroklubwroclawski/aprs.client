﻿using System;
using System.Globalization;

namespace APRS.Client.Filters
{
  public class RangeFilter : IFilter
  {
    private readonly float _latitude;

    private readonly float _longitude;

    private readonly int _distance;

    /// <summary>
    /// The range filter will pass all stations and objects within a distance from
    /// </summary>
    /// <param name="latitude">Negative SOUTH</param>
    /// <param name="longitude">Negative WEST</param>
    /// <param name="distance">Unit kilometers</param>
    public RangeFilter(float latitude, float longitude, int distance)
    {
      _latitude = latitude;
      _longitude = longitude;
      _distance = distance;
    }

    public string GetFilterString()
    {
      return string.Format(new CultureInfo("en-US"), "r/{0}/{1}/{2}", _latitude, _longitude, _distance);
    }
  }
}