﻿namespace APRS.Client.Filters
{
  public interface IFilter
  {
    string GetFilterString();
  }
}