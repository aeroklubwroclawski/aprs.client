﻿using System;

namespace APRS.Client.Filters
{
  public class TypeFilter : IFilter
  {
    private readonly bool _position;

    private readonly bool _objects;

    private readonly bool _items;

    private readonly bool _message;

    private readonly bool _nwsWeatherAndObjects;

    private readonly bool _weather;

    private readonly bool _telemetry;

    private readonly bool _query;

    private readonly bool _status;

    private readonly bool _userDefined;

    private readonly string _callsign;

    private readonly int? _distance;

    public TypeFilter(
      bool position,
      bool objects,
      bool items,
      bool message,
      bool nwsWeatherAndObjects,
      bool weather,
      bool telemetry,
      bool query,
      bool status,
      bool userDefined,
      string callsign = null,
      int? distance = null)
    {
      if (string.IsNullOrEmpty(callsign) == false && distance.HasValue == false)
      {
        throw new ArgumentNullException(nameof(distance), "Distance can't be null when callsign is defined");
      }

      _position = position;
      _objects = objects;
      _items = items;
      _message = message;
      _nwsWeatherAndObjects = nwsWeatherAndObjects;
      _weather = weather;
      _telemetry = telemetry;
      _query = query;
      _status = status;
      _userDefined = userDefined;
      _callsign = callsign;
      _distance = distance;
    }

    public string GetFilterString()
    {
      var result = "t/";

      if (_position) result += "p";
      if (_objects) result += "o";
      if (_items) result += "i";
      if (_message) result += "m";
      if (_nwsWeatherAndObjects) result += "n";
      if (_weather) result += "w";
      if (_telemetry) result += "t";
      if (_query) result += "q";
      if (_status) result += "s";
      if (_userDefined) result += "u";

      if (string.IsNullOrEmpty(_callsign) == false) result += $"/{_callsign}/{_distance}";

      return result;
    }
  }
}