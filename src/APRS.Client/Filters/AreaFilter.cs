﻿namespace APRS.Client.Filters
{
  public class AreaFilter : IFilter
  {
    private readonly float _latitudeNorth;

    private readonly float _longitudeWest;

    private readonly float _latitudeSouth;

    private readonly float _longitudeEast;

    public AreaFilter(float latitudeNorth, float longitudeWest, float latitudeSouth, float longitudeEast)
    {
      _latitudeNorth = latitudeNorth;
      _longitudeWest = longitudeWest;
      _latitudeSouth = latitudeSouth;
      _longitudeEast = longitudeEast;
    }

    public string GetFilterString()
    {
      return $"a/{_latitudeNorth}/{_longitudeWest}/{_latitudeSouth}/{_longitudeEast}";
    }
  }
}