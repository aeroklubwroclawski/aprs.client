﻿namespace APRS.Client.Filters
{
  public class TextFilter : IFilter
  {
    private readonly string  _filterText;

    public TextFilter(string filterText)
    {
      _filterText = filterText;
    }

    public string GetFilterString()
    {
      return _filterText;
    }
  }
}