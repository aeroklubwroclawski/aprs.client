﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.Event;
using Akka.Logger.Serilog;
using APRS.Client.Filters;

namespace APRS.Client.Actors
{
  public class AprsTcpClient : ReceiveActor, ILogReceive
  {
    #region Messages

    public class Connect
    {
      public string Host { get; }

      public int Port { get; }

      public string Username { get; }

      public string Password { get; }

      public IFilter Filter { get; }

      public Connect(string host, int port, string username, string password, IFilter filter)
      {
        Host = host;
        Port = port;
        Username = username;
        Password = password;
        Filter = filter;
      }
    }

    public class Disconnect
    {
    }

    private class StartRecieving
    {
    }

    public class ReceivedMessage
    {
      public string Message { get; }

      public ReceivedMessage(string message)
      {
        Message = message;
      }
    }

    private class SendKeepAlive
    {
    }

    internal class StreamFaulted
    {
    }

    #endregion

    private readonly ILoggingAdapter _log = Context.GetLogger(new SerilogLogMessageFormatter());
    private readonly IActorRef _tncDecoder;

    private TcpClient _tcpClient;
    private Stream _stream;
    private CancellationTokenSource _streamListenerCancel;
    private ICancelable _cancelKeepAlive;
    private string _host;
    private int _port;
    private string _username;
    private string _password;
    private IFilter _filter;

    public AprsTcpClient()
    {
      _tncDecoder = Context.ActorOf<Tnc2Decoder>("tnc2-decoder");

      Disconnected();
    }

    private void Disconnected()
    {
      Receive<Connect>(message =>
      {
        _log.Info("Connecting to APRS TCP endpont");

        StoreConnectionData(message);
        ConnectToAprs();

        Self.Tell(new StartRecieving());

        Become(Connected);
      });
    }

    private void Connected()
    {
      Receive<StartRecieving>(message =>
      {
        _log.Info("Start receiving APRS packets");

        _cancelKeepAlive = new Cancelable(Context.System.Scheduler);
        _streamListenerCancel = new CancellationTokenSource();

        StartListenerTask(_tcpClient.GetStream(), _tncDecoder);

        Context.System.Scheduler.ScheduleTellRepeatedly(
          TimeSpan.FromMinutes(1),
          TimeSpan.FromMinutes(1),
          Self, new SendKeepAlive(), ActorRefs.Nobody,
          _cancelKeepAlive);
      });

      Receive<ReceivedMessage>(message =>
      {
        _log.Info($"Message: {message.Message}");
      });

      Receive<SendKeepAlive>(message =>
      {
        _log.Debug("Send keep alive");

        const string keepAliveMessage = "# APRS.Client";
        var data = Encoding.ASCII.GetBytes(keepAliveMessage);

        try
        {
          _stream.Write(data, 0, data.Length);
        }
        catch (Exception)
        {
          _log.Warning("Stream faulted while trying to send keep alive");

          Self.Tell(new StreamFaulted());

          Become(Faulted);
        }
      });

      Receive<Disconnect>(message =>
      {
        _log.Info("Disconnect from TCP APRS stream");

        DisconnectFromAprsStream();

        Become(Disconnected);
      });
    }

    private void Faulted()
    {
      Receive<StreamFaulted>(message =>
      {
        _log.Info("Handling stream faulted");

        DisconnectFromAprsStream();

        ConnectToAprs();

        Self.Tell(new StartRecieving());

        Become(Connected);
      });
    }

    private void StoreConnectionData(Connect message)
    {
      _host = message.Host;
      _port = message.Port;
      _username = message.Username;
      _password = message.Password;
      _filter = message.Filter;
    }

    private void ConnectToAprs()
    {
      _log.Info("Creating TCP connection to APRS stream");

      _tcpClient = new TcpClient(_host, _port);
      _stream = _tcpClient.GetStream();

      LoginToAprs(_username, _password, _filter);
    }

    private void LoginToAprs(string username, string password, IFilter filter)
    {
      using (var sw = new StreamWriter(_tcpClient.GetStream(), Encoding.ASCII, 4096, true))
      {
        var handshake = $"user {username} pass {password} vers AprsClient 0.0.1 filter {filter.GetFilterString()}";

        _log.Debug($"Handshake: {handshake}");

        sw.WriteLine(handshake);
        sw.Flush();
      }
    }

    private void StartListenerTask(Stream stream, IActorRef decoder)
    {
      Task
        .Run(ProcessAprsStream(stream, decoder), _streamListenerCancel.Token)
        .ContinueWith(OnProcessingCancelled(), TaskContinuationOptions.ExecuteSynchronously);
    }

    private Action ProcessAprsStream(Stream stream, IActorRef decoder)
    {
      return () =>
      {
        _log.Debug("Start listening for packets");

        using (var sr = new StreamReader(stream, Encoding.ASCII))
        {
          while (_streamListenerCancel.IsCancellationRequested == false && stream.CanRead)
          {
            var line = sr.ReadLine();

            if (string.IsNullOrEmpty(line) == false)
            {
              decoder.Tell(new Tnc2Decoder.DecodeMessage(line));
            }
          }
        }
      };
    }

    private Action<Task> OnProcessingCancelled()
    {
      return x =>
      {
        if (x.IsFaulted)
        {
          _log.Warning("Stream faulted on listener task");

          Self.Tell(new StreamFaulted());

          Become(Faulted);
        }
        else if (x.IsCanceled)
        {
          _log.Info("Listener task was canceled");
        }
      };
    }

    private void DisconnectFromAprsStream()
    {
      _cancelKeepAlive.Cancel();
      _streamListenerCancel.Cancel();

      _tcpClient.Close();
      _tcpClient.Dispose();
    }
  }
}