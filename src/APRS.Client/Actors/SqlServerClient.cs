﻿using Akka.Actor;
using APRS.Client.DataModel;
using APRS.Client.Events;
using APRS.Client.Model;

namespace APRS.Client.Actors
{
  public class SqlServerClient : ReceiveActor, ILogReceive
  {
    public SqlServerClient()
    {
      Context.System.EventStream.Subscribe(Self, typeof(AprsPositionPacketDecodedEvent));
      Context.System.EventStream.Subscribe(Self, typeof(UnsupportedPacketReceivedEvent));

      Receive<AprsPositionPacketDecodedEvent>(message =>
      {
        using (var context = new AprsContext())
        {
          var entity = TrackingPoint.FromAprsPacket(message.Packet);

          context.TrackingPoints.Add(entity);

          context.SaveChanges();
        }
      });

      Receive<UnsupportedPacketReceivedEvent>(message =>
      {
        using (var context = new AprsContext())
        {
          var entity = new UnsupportedPacket(message.Payload);

          context.UnsupportedPackets.Add(entity);

          context.SaveChanges();
        }
      });
    }
  }
}