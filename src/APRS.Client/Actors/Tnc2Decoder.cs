﻿using System;
using Akka.Actor;
using Akka.Event;
using Akka.Logger.Serilog;
using APRS.Client.Events;
using APRS.Client.Model;

namespace APRS.Client.Actors
{
  public class Tnc2Decoder : ReceiveActor, ILogReceive
  {
    #region Messages

    public class DecodeMessage
    {
      public string Message { get; }

      public DecodeMessage(string message)
      {
        Message = message;
      }
    }

    #endregion

    private readonly ILoggingAdapter _log = Context.GetLogger(new SerilogLogMessageFormatter());

    public Tnc2Decoder()
    {
      Receive<DecodeMessage>(message =>
      {
        _log.Debug("Decoding message [{@message}]", message.Message);

        AprsPacket packet;

        if (TryParseAprsPacket(message, out packet) == false)
        {
          return;
        }

        if (packet.Type == AprsPacketType.Position)
        {
          Context.System.EventStream.Publish(new AprsPositionPacketDecodedEvent(packet));
        }
      });
    }

    private bool TryParseAprsPacket(DecodeMessage message, out AprsPacket packet)
    {
      try
      {
        packet = new AprsPacket(message.Message);
      }
      catch (Exception e)
      {
        _log.Error(e, "Unsupported APRS packet");
        packet = null;
        Context.System.EventStream.Publish(new UnsupportedPacketReceivedEvent(message.Message));
        return false;
      }

      return true;
    }
  }
}