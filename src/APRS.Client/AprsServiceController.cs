﻿using Akka.Actor;
using APRS.Client.Actors;
using APRS.Client.Configuration;
using APRS.Client.Filters;

namespace APRS.Client
{
  public class AprsServiceController
  {
    private ActorSystem _actorSystem;
    private IActorRef _aprsClient;
    private IActorRef _sqlServerClient;

    public void Start()
    {
      _actorSystem = ActorSystem.Create("AprsClient");

      var aprsSettings = new AprsSettings();
      _aprsClient = _actorSystem.ActorOf<AprsTcpClient>("aprs-client");
      _sqlServerClient = _actorSystem.ActorOf<SqlServerClient>("sql-server-client");

      IFilter filter = new AreaFilter(54.631778f, 13.425321f, 48.918050f, 24.126005f);

      if (string.IsNullOrEmpty(aprsSettings.Filter) == false)
      {
        filter = new TextFilter(aprsSettings.Filter);
      }

      _aprsClient.Tell(new AprsTcpClient.Connect(
        aprsSettings.Host,
        aprsSettings.Port,
        aprsSettings.Login,
        aprsSettings.Password,
        filter
      ));
    }

    public void Stop()
    {
      _aprsClient.Tell(new AprsTcpClient.Disconnect());

      _actorSystem.Stop(_aprsClient);
      _actorSystem.Stop(_sqlServerClient);

      _actorSystem.Terminate();
    }
  }
}