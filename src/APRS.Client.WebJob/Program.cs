﻿using System;
using APRS.Client.Configuration;
using APRS.Migrations.Runners;
using Microsoft.Azure.WebJobs;
using Serilog;
using Serilog.Exceptions;

namespace APRS.Client.WebJob
{
  // To learn more about Microsoft Azure WebJobs SDK, please see http://go.microsoft.com/fwlink/?LinkID=320976
  class Program
  {
    // Please set the following connection strings in app.config for this WebJob to run:
    // AzureWebJobsDashboard and AzureWebJobsStorage
    static void Main()
    {
      Log.Logger = new LoggerConfiguration()
        .Enrich.FromLogContext()
        .Enrich.WithThreadId()
        .Enrich.WithExceptionDetails()
        .WriteTo.Trace()
        .MinimumLevel.Verbose()
        .CreateLogger();

      var config = new JobHostConfiguration();
      config.UseSerilog();

      var host = new JobHost(config);

      try
      {
        RuntimeMigrationRunner.RunMigrations(new SqlServerConfiguration().ConnectionString);
      }
      catch (Exception e)
      {
        Log.Logger.Error(e, "Cannot migrate database to newest version");
        throw;
      }

      var aprsHost = new AprsServiceController();

      aprsHost.Start();

      // The following code ensures that the WebJob will be running continuously
      host.RunAndBlock();

      aprsHost.Stop();
    }
  }
}
