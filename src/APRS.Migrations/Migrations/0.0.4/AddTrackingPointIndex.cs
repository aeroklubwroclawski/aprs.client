﻿using FluentMigrator;

namespace APRS.Migrations.Migrations
{
  [Migration(4)]
  public class AddTrackingPointIndex : Migration
  {
    public override void Up()
    {
      Execute.Sql("CREATE NONCLUSTERED INDEX [IDX_TrackingPoint_Callsign_Time] ON [dbo].[TrackingPoints] ([Callsign], [Time]) INCLUDE ([Altitude], [Latitude], [Longitude])");
    }

    public override void Down()
    {
      Delete.Index("IDX_TrackingPoint_Callsign_Time");
    }
  }
}
