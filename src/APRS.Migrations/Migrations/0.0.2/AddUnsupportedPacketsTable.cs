﻿using FluentMigrator;

namespace APRS.Migrations.Migrations
{
  [Migration(2)]
  public class AddUnsupportedPacketsTable : AutoReversingMigration
  {
    public override void Up()
    {
      Create.Table("UnsupportedPackets")
        .WithColumn("Id").AsGuid().NotNullable().PrimaryKey().WithDefault(SystemMethods.NewGuid)
        .WithColumn("Payload").AsString().NotNullable()
        .WithColumn("CreatedDate").AsDateTime().NotNullable();
    }
  }
}