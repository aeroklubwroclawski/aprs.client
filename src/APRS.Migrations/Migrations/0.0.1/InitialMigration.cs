﻿using FluentMigrator;

namespace APRS.Migrations.Migrations
{
  [Migration(1)]
  public class InitialMigration : AutoReversingMigration
  {
    public override void Up()
    {
      Create.Table("TrackingPoints")
        .WithColumn("Id").AsGuid().NotNullable().PrimaryKey().WithDefault(SystemMethods.NewGuid)
        .WithColumn("Callsign").AsString().NotNullable().Indexed()
        .WithColumn("Time").AsDateTime().NotNullable().Indexed()
        .WithColumn("Latitude").AsFloat().NotNullable()
        .WithColumn("Longitude").AsFloat().NotNullable()
        .WithColumn("Altitude").AsFloat().NotNullable()
        .WithColumn("CreatedDate").AsDateTime().NotNullable();
    }
  }
}