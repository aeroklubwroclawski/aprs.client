﻿using FluentMigrator;

namespace APRS.Migrations.Migrations
{
  [Migration(3)]
  public class AddIndexesToTrackingPointsTable : AutoReversingMigration
  {
    public override void Up()
    {
      Create.Index("IX_TrackingPoints_CreatedDate")
        .OnTable("TrackingPoints")
        .OnColumn("CreatedDate");
    }
  }
}