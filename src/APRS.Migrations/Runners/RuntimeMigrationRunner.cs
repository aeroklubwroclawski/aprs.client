﻿using System.Reflection;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Announcers;
using FluentMigrator.Runner.Initialization;
using FluentMigrator.Runner.Processors;
using FluentMigrator.Runner.Processors.SqlServer;
using Serilog;

namespace APRS.Migrations.Runners
{
  public static class RuntimeMigrationRunner
  {
    private static readonly ILogger Log = Serilog.Log.Logger;

    public static void RunMigrations(string connectionString)
    {
      Log.Information("Starting database migrations");

      var announcer = new TextWriterAnnouncer(s => Log.Information(s));
      var assembly = Assembly.GetExecutingAssembly();
      var processorOptions = new ProcessorOptions
      {
        PreviewOnly = false,
        Timeout = 300
      };

      var context = new RunnerContext(announcer);

      var factory = new SqlServer2012ProcessorFactory();

      using (var migrationProcessor = factory.Create(connectionString, announcer, processorOptions))
      {
        var runner = new MigrationRunner(assembly, context, migrationProcessor);

        runner.MigrateUp(true);
      }

      Log.Information("Migrations executed successfuly");
    }
  }
}