# README #

This is TCP client of APRS network to download packets from OGN network.

This service is based on Akka.NET actor model to quickly parse and save packets form network.

This microservice can be run as:

* console application using `APRS.NTService.exe` file
* windows service using `APRS.NTService.exe` file
* Azure WebJob using `APRS.Client.WebJob` project

## Installation

To use this application you have to provide connection string to existing SQL Server database, and configure filter for APRS packets.

Depending on usage, those parameters have to be set in `APRS.NTService/app.config` file or `APRS.Client.WebJob/app.config` file.