properties {
$migratorPath = './tools/fluentmigrator/migrate.exe'
  $connectionString = 'Data Source=(localdb)\ProjectsV13;Initial Catalog=APRS.Client;Integrated Security=True;MultipleActiveResultSets=true;'
  $migrationsAssembly = './bin/APRS.Migrations.dll'
}

task MigrateDown {
  Exec{
    iex "$migratorPath -a '$migrationsAssembly' -db SqlServer2012 -conn '$connectionString' -t migrate:down --verbose=true"
  }
}

task MigrateUp {
  Exec{
    iex "$migratorPath -a '$migrationsAssembly' -db SqlServer2012 -conn '$connectionString' -t migrate:up --verbose=true"
  }
}